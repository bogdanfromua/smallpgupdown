package com.plb.smallpgupdown;

import java.util.ArrayList;

public class JumpLogic {

    public static final String NEW_LINE_SYMBOL = "\n";
    
    private static int getCurrentLineIndex(String text, ArrayList<Integer> newLinesPositions, int initialPosition) {
        if (newLinesPositions.size() == 0) {
            return 0;        
        }
        
        for (int i = newLinesPositions.size()-1; i>=0; i--) {
            if (newLinesPositions.get(i) <= initialPosition) {
                return i + 1;
            }
        }

        return 0;
    }

    private static int getOffsetFromLineStart(String text, ArrayList<Integer> newLinesPositions, int initialPosition) {
        int lineIndex = getCurrentLineIndex(text, newLinesPositions, initialPosition);
        Integer lineStart = getLineStartPosition(text, newLinesPositions, lineIndex);
        return initialPosition - lineStart;
    }
    
    private static int getLineStartPosition(String text, ArrayList<Integer> newLinesPositions, int lineIndex) {
        if (0 == lineIndex) {
            return 0;
        } else {
            return newLinesPositions.get(lineIndex - 1);
        }

    }
    
    private static int getLastLineIndex(String text, ArrayList<Integer> newLinesPositions) {
        return newLinesPositions.size();
    }
    
    private static int getLineLength(String text, ArrayList<Integer> newLinesPositions, int targetLineIndex) {
        final Integer lineStart = getLineStartPosition(text, newLinesPositions, targetLineIndex);
        
        int lastLineIndex = getLastLineIndex(text, newLinesPositions);
        
        final int lineEnd;
        if (lastLineIndex == targetLineIndex) {
            lineEnd = text.length();
        } else {
            lineEnd = newLinesPositions.get(targetLineIndex + 1);
        }
        
        return lineEnd - lineStart;
    }
    
    public static int calculateAnotherLinePosition(String text, int initialPosition, int linesOffset) {
        // prepare text
        ArrayList<Integer> newLinesPositions = new ArrayList<>();
        int index = text.indexOf(NEW_LINE_SYMBOL);
        while (index >= 0) {
            newLinesPositions.add(index);
            index = text.indexOf("\n", index + 1);
        }
        
        // get initial position info
        int currentLineIndex = getCurrentLineIndex(text, newLinesPositions, initialPosition);
        int initialInlineOffset = getOffsetFromLineStart(text, newLinesPositions, initialPosition);

        // get target line
        int targetLineIndex = currentLineIndex + linesOffset;

        // check target line out of bounds
        int lastLineIndex = getLastLineIndex(text, newLinesPositions);
        if (targetLineIndex < 0) {
            targetLineIndex = 0;
        }
        if (targetLineIndex > lastLineIndex) {
            targetLineIndex = lastLineIndex;
        }
        
        // get offset in target line
        int targetLineLength = getLineLength(text, newLinesPositions, targetLineIndex);
        int targetInlineOffset = targetLineLength < initialInlineOffset
                ? targetLineLength
                : initialInlineOffset;

        // get absolute target position
        int targetLinePosition = getLineStartPosition(text, newLinesPositions, targetLineIndex);
        return targetLinePosition + targetInlineOffset;
    }
}
