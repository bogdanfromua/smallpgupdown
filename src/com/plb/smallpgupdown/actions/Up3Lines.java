package com.plb.smallpgupdown.actions;

import com.plb.smallpgupdown.JumpExecutor;
import com.plb.smallpgupdown.JumpLogic;

public class Up3Lines extends AbstractJumpAction {
    
    @Override
    public void jumpAction(JumpExecutor jumpExecutor, int initialOffset, String text) {
        jumpExecutor.jump(JumpLogic.calculateAnotherLinePosition(text, initialOffset, -3));
    }
}
