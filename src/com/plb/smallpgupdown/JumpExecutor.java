package com.plb.smallpgupdown;

import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.ScrollType;

public class JumpExecutor {

    private Editor editor;
    private CaretModel caretModel;

    public JumpExecutor(Editor editor, CaretModel caretModel) {
        this.editor = editor;
        this.caretModel = caretModel;
    }

    public void jump(Integer position) {
        jump(position, 0);
    }

    public void jump(Integer position, int additionalOffset) {
        if (null != position) {
            caretModel.moveToOffset(position + additionalOffset);
            editor.getScrollingModel().scrollToCaret(ScrollType.MAKE_VISIBLE);
        }
    }
}
